const Prices = () => {
  return (
    <div className="p-4 lg:px-32 lg:mb-20 lg:mt-10">
      <div className="text-center">
        <p className="mt-7 mb-3 font-bold text-xs lg:text-4xl lg:mb-5">
          Honest prices, no hidden fees.
        </p>
        <p className="text-xs mb-5 lg:text-xl">
          We provide you with endless supply of knowledge with complete
          financial transparency. Less money, more videos
        </p>
      </div>
      <div className="flex justify-between md:justify-center">
        <div className="relative w-32 h-40 mr-7 rounded bg-gradient-to-b from-blue-light to-blue-dark lg:w-48 lg:h-60">
          <div className="ml-2.5 text-white">
            <p className="text-4xl font-black lg:text-7xl">5</p>
            <p className="uppercase text-lg font-semibold lg:text-2xl">Seats</p>
            <div className="absolute bottom-0">
              <p className="text-lg font-semibold lg:hidden">
                $490.<span className="text-xs font-semibold">00 MXN</span>
              </p>
              <p className="text-xs font-semibold mb-2.5 lg:hidden">
                Billed Monthly
              </p>
              <p className="hidden text-sm font-light lg:block">Mex$</p>
              <p className="hidden text-4xl font-medium lg:block">120.88</p>
              <p className="hidden text-sm font-light mb-2.5 lg:block">
                Billed Monthly
              </p>
            </div>
          </div>
        </div>
        <div className="relative float-right w-32 h-40 rounded bg-gradient-to-b from-red-light to-red-dark lg:w-48 lg:h-60">
          <div className="ml-2.5 text-white">
            <p className="text-4xl font-black lg:text-7xl">1</p>
            <p className="uppercase text-lg font-semibold lg:text-2x">Seat</p>
            <div className="absolute bottom-0">
              <p className="text-lg font-semibold lg:hidden">
                $490.<span className="text-xs font-semibold">00 MXN</span>
              </p>
              <p className="text-xs font-semibold mb-2.5 lg:hidden">
                Billed Monthly
              </p>
              <p className="hidden text-sm font-light lg:block">Mex$</p>
              <p className="hidden text-4xl font-medium lg:block">25.90</p>
              <p className="hidden text-sm font-light mb-2.5 lg:block">
                Billed Monthly
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden justify-center items-center lg:flex">
        <div className="flex justify-center items-center rounded bg-black h-12 w-64 mt-8">
          <div className="text-white capitalize font-semibold text-xl">
            Start for free
          </div>
        </div>
      </div>
    </div>
  );
};

export default Prices;
