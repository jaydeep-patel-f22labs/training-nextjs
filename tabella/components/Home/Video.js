const Video = () => {
  return (
    <div className="bg-theme-grey mt-5">
      <div className="mx-4 md:mx-20">
        <div className="text-center font-bold text-xs pt-6 mb-5 lg:text-4xl lg:mb-7 lg:pt-20">
          <p>Take a look at what we have to offer</p>
        </div>
        <div className="hidden lg:block">
          <p className="text-xl text-center mb-14">
            Tabella follows a comprehensive method of learn, practice and test
            for deep immersive learning. It uses various tools like animations,
            videos and virtual labs to make learning easy and engaging. Our
            courses are designed to be entertaining, challenging, and
            educational. They are for ambitious students, professionals, and
            lifelong learners.
          </p>
        </div>
        <div className="relative lg:flex lg:justify-center">
          <video className="lg:w-3/5">
            <source src="https://cdn.shopify.com/s/files/1/0042/2405/7462/files/Parker_Flipped.mov?1222"></source>
          </video>
          <div className="absolute top-0 right-0 lg:hidden ">
            <div className="">
              <img src="img/stacked-group-2.svg" alt="white-triangle" />
            </div>
          </div>
        </div>

        <div className="mt-3 pb-8 flex justify-center lg:mt-14 lg:pb-20">
          <img
            src="/img/34767.svg"
            alt="img11"
            className="h-9 w-14 mr-3.5 lg:mr-5 lg:w-28 lg:h-20"
          />
          <img
            src="/img/34767.svg"
            alt="img12"
            className="h-9 w-14 mr-3.5 lg:mr-5 lg:w-28 lg:h-20"
          />
          <img
            src="/img/34767.svg"
            alt="img13"
            className="h-9 w-14 mr-3.5 lg:mr-5 lg:w-28 lg:h-20"
          />
        </div>
      </div>
    </div>
  );
};

export default Video;
