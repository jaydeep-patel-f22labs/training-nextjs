const HeroSection = () => {
  return (
    <div className="mx-4 md:mx-0">
      {/* mobile */}
      <div className="flex items-center justify-between flex-col lg:hidden">
        <div className="mt-20">
          <img src="img/34767.svg" alt="img6" className="h-auto w-auto" />
        </div>
        <div className=" w-1/2 font-bold mt-9">
          <p className="text-sm text-center font-sans1">
            LEARNING MADE EASY & ENGAGING
          </p>
        </div>
        <div>
          <p className="text-xs mt-3.5 text-center mb-5">
            Tabella helps you learn more effectively than lectures through
            interactive problem solving courses. Our courses are designed to be
            entertaining, challenging, and educational. They are for ambitious
            students, professionals, and lifelong learners.
          </p>
        </div>
        <div className="flex justify-center items-center rounded bg-black h-9 w-36">
          <div className="text-white capitalize font-semibold text-base">
            try tabbella
          </div>
        </div>
      </div>

      {/* laptop */}
      <div className="hidden lg:flex">
        <div className="flex-1 flex justify-center items-center">
          <div className="w-2/3 text-left">
            <p className="tracking-wide uppercase text-4xl font-Poppins">
              LEARNING MADE EASY & ENGAGING
            </p>
            <p className="text-xl font-medium mt-5">
              Tabella helps you learn more effectively than lectures through
              interactive problem solving courses. Our courses are designed to
              be entertaining, challenging, and educational. They are for
              ambitious students, professionals, and lifelong learners.
            </p>
            <div className="flex justify-center items-center rounded bg-black h-12 w-64 mt-8">
              <div className="text-white capitalize font-semibold text-lg">
                try tabbella for free
              </div>
            </div>
          </div>
        </div>
        <div className="flex-1 bg-hero-pattern">
          <div className="mx-20 ">
            <img
              src="/img/34767.svg"
              alt="img7"
              className="h-screen w-auto object-contain"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeroSection;
