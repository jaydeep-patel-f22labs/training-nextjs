import Head from "next/head";
import Header from "../../components/Home/Header";
import HeroSection from "../../components/Home/HeroSection";
import Video from "../../components/Home/Video";
import Mobile from "../../components/Home/Mobile";
import Gurus from "../../components/Home/Gurus";
import Prices from "../../components/Home/Prices";
import Learn from "../../components/Home/Learn";
import Footer from "../../components/Home/Footer";

const Home = () => {
  return (
    <>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
        <meta charset="UTF-8" />
        <meta name="description" content="Free Web tutorials" />
        <meta name="keywords" content="HTML,CSS,XML,JavaScript" />
        <meta name="author" content="John Doe" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <div className="font-ProximaNova">
        <Header />
        <HeroSection />
        <Video />
        <Mobile />
        <Gurus />
        <Prices />
        <Learn />
        <Footer />
      </div>
    </>
  );
};

export default Home;
