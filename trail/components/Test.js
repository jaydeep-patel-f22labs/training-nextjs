import React, { useState } from "react";
import { useVideo } from "react-use";

const VideoT = () => {
  const [video, state, controls, ref] = useVideo(
    <video src="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" />
  );

  const [showSidebar, setSidebar] = useState(false);

  return (
    <div>
      {video}

      <button
        onClick={() => {
          setSidebar(!showSidebar)
          
        }}
      >
        {showSidebar ? (
        <div>
          <button onClick={controls.pause}>Pause</button>
        </div>
      ) : (
        <div>
          <button onClick={controls.play}>Play</button>
        </div>
      )}

      </button>

      {showSidebar ? (
        <div>
          <button onClick={controls.pause}>Pause</button>
        </div>
      ) : (
        <div>
          <button onClick={controls.play}>Play</button>
        </div>
      )}

      <br />
      <button onClick={controls.mute}>Mute</button>
      <button onClick={controls.unmute}>Un-mute</button>
      <br />
      <button onClick={() => controls.volume(0.1)}>Volume: 10%</button>
      <button onClick={() => controls.volume(0.5)}>Volume: 50%</button>
      <button onClick={() => controls.volume(1)}>Volume: 100%</button>
      <br />
      <button onClick={() => controls.seek(state.time - 5)}>-5 sec</button>
      <button onClick={() => controls.seek(state.time + 5)}>+5 sec</button>
    </div>
  );
};

export default VideoT;
