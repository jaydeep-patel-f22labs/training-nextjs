import React from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
import Image from "next/image";

const handleDragStart = (e) => e.preventDefault();

const items = [
  <div
    className="flex justify-center items-center flex-col m-4 lg:mx-14 lg:w-11/12"
    onDragStart={handleDragStart}
  >
    <p className="mt-7 mb-3 font-bold text-xs lg:text-4xl lg:mb-5 lg:mt-16">
      Why Learn on Tabella 1
    </p>
    <p className="text-xs w-72 mb-5 text-center lg:text-xl lg:w-11/12 lg:px-5">
      Tabella follows a comprehensive method of learn, practice and test for
      deep immersive learning. It uses various tools like animations, videos and
      virtual labs to make learning easy and engaging. Our courses are designed
      to be entertaining, challenging, and educational. They are for ambitious
      students, professionals, and lifelong learners.
    </p>
  </div>,
  <div
    className="flex justify-center items-center flex-col m-4 lg:mx-14 lg:w-11/12"
    onDragStart={handleDragStart}
  >
    <p className="mt-7 mb-3 font-bold text-xs lg:text-4xl lg:mb-5 lg:mt-16">
      Why Learn on Tabella 2
    </p>
    <p className="text-xs w-72 mb-5 text-center lg:text-xl lg:w-11/12 lg:px-5">
      Tabella follows a comprehensive method of learn, practice and test for
      deep immersive learning. It uses various tools like animations, videos and
      virtual labs to make learning easy and engaging. Our courses are designed
      to be entertaining, challenging, and educational. They are for ambitious
      students, professionals, and lifelong learners.
    </p>
  </div>,
  <div
    className="flex justify-center items-center flex-col m-4 lg:mx-14 lg:w-11/12"
    onDragStart={handleDragStart}
  >
    <p className="mt-7 mb-3 font-bold text-xs lg:text-4xl lg:mb-5 lg:mt-16">
      Why Learn on Tabella 3
    </p>
    <p className="text-xs w-72 mb-5 text-center lg:text-xl lg:w-11/12 lg:px-5">
      Tabella follows a comprehensive method of learn, practice and test for
      deep immersive learning. It uses various tools like animations, videos and
      virtual labs to make learning easy and engaging. Our courses are designed
      to be entertaining, challenging, and educational. They are for ambitious
      students, professionals, and lifelong learners.
    </p>
  </div>,
  <div
    className="flex justify-center items-center flex-col m-4 lg:mx-14 lg:w-11/12"
    onDragStart={handleDragStart}
  >
    <p className="mt-7 mb-3 font-bold text-xs lg:text-4xl lg:mb-5 lg:mt-16">
      Why Learn on Tabella 4
    </p>
    <p className="text-xs w-72 mb-5 text-center lg:text-xl lg:w-11/12 lg:px-5">
      Tabella follows a comprehensive method of learn, practice and test for
      deep immersive learning. It uses various tools like animations, videos and
      virtual labs to make learning easy and engaging. Our courses are designed
      to be entertaining, challenging, and educational. They are for ambitious
      students, professionals, and lifelong learners.
    </p>
  </div>,
];

const Mobile = ({ mobileImg }) => {
  mobileImg = {
    width: 102,
    height: 206,
    src: "/img/aaaaaaaaa-3-x.jpg",
    alt: "mobile img",
  };

  const renderDotsItem = ({ isActive }) => {

    

    return(
      <div>
        {/* isActive ? 'a' : 'o'; */}
       
        <div className='mb-12'>
        <div className={`${isActive ? 'h-3 w-3 bg-black rounded-full mr-3.5' : 'h-3 w-3 bg-gray-300 rounded-full mr-3.5'}`}></div>
        </div>
       
      </div>
    ) 

}; 

  return (
    <>
      {/* carosoul mobile */}
      <span className="block lg:hidden">
        <AliceCarousel
          disableDotsControls
          disableButtonsControls
          items={items}
          
        />
      </span>

      {/* laptop */}
     
      <span className="hidden lg:block">
        <AliceCarousel
          mouseTracking
          autoPlayStrategy="none"
          autoPlay
          infinite
          autoPlayInterval="3000"
          disableButtonsControls
          renderDotsItem={renderDotsItem}
          items={items}
        />
      </span>
      <div className="flex justify-center items-center flex-col lg:mx-32">
        <img
          src={mobileImg.src}
          width={mobileImg.width}
          height={mobileImg.height}
          alt={mobileImg.alt}
          layout="responsive"
          className="w-24 h-48  mt-0 lg:-ml-5 lg:w-80 lg:h-auto"
        />
      </div>
    </>
  );
};

export default Mobile;
