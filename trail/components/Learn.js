import Image from "next/image";

const Learn = ({ computer, google }) => {
  computer = {
    width: 193,
    height: 85,
    src: "/img/group-10-3-x.jpg",
    alt: "computer img",
    srcSet: "img/group-10-3-x@2x.jpg 2x, img/group-10-3-x@3x.jpg 3x",
  };

  google = {
    width: 94.3,
    height: 28.3,
    src: "/img/google-play-badge-3-x.jpg",
    alt: "playstore img",
    srcSet:
      "img/google-play-badge-3-x@2x.jpg 2x, img/google-play-badge-3-x@3x.jpg 3x",
  };

  return (
    <div className="p-4 bg-theme-grey mt-9 lg:px-32 lg:mb-28">
      <div className="mt-8 flex justify-center items-center flex-col lg:flex-row">
        <div className="w-48 h-auto lg:w-full object-contain lg:mr-36">
          <Image
            src={computer.src}
            width={computer.width}
            height={computer.height}
            alt={computer.alt}
            srcSet={computer.srcSet}
            layout="responsive"
          />
        </div>

        <div className="flex justify-center items-center flex-col lg:items-start">
          <p className="mt-7 mb-3 font-bold text-xs lg:mt-0 lg:text-4xl lg:mb-5 lg:text-left">
            Learn Anywhere
          </p>
          <p className="text-xs w-72 mb-5 text-center lg:text-xl lg:w-full lg:text-left">
            Make use of the technology available and access Tabella everywhere,
            be it your home or inside the Mariana Trench. It is available on
            both Android and iOS platforms.
          </p>
          <div className="flex justify-center items-center lg:justify-start">
            <div className=" w-24 my-3.5 mr-1 lg:ml-0 lg:my-0 lg:w-40 lg:h-16">
              <Image
                src={google.src}
                width={google.width}
                height={google.height}
                alt={google.alt}
                srcSet={google.srcSet}
                layout="responsive"
              />
            </div>
            <div className=" w-24 my-3.5 mr-1 lg:w-40 lg:h-16">
              <Image
                src={google.src}
                width={google.width}
                height={google.height}
                alt={google.alt}
                srcSet={google.srcSet}
                layout="responsive"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Learn;
