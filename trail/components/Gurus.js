import Image from "next/image";

function GurusImg(props) {
  const content = props.posts.map((post) => (
    <div
      key={post.index}
      className="h-20 w-28  lg:w-72 lg:h-full"
    >
      <Image
        src={post.src}
        width={post.width}
        height={post.height}
        alt={post.alt}
        layout="responsive"
      />
    </div>
  ));
  return (
    <>
      {" "}
      <div className="flex justify-center lg:mt-14">
        <div className="grid grid-rows-2 grid-flow-col gap-4 lg:gap-5">{content}</div>
      </div>
    </>
  );
}

const posts = [
  { index: 1, src: "/img/1.jpg", alt: "guru img1", width: 108, height: 80 },
  { index: 2, src: "/img/s2.jpg", alt: "guru img2", width: 108, height: 80 },
  { index: 3, src: "/img/3.jpg", alt: "guru img3", width: 108, height: 80 },
  { index: 4, src: "/img/4.jpg", alt: "guru img4", width: 108, height: 80 },
];

const Gurus = () => {
  return (
    <div className="-mt-16 inline-block w-screen">
      <div className="bg-theme-grey lg:py-20">
        <div className="p-4 lg:px-32">
          <div className="flex justify-center items-center flex-col">
            <p className="mt-1 mb-3 font-bold text-xs lg:text-4xl lg:mb-5 ">
              Let us introduce you to our Gurus
            </p>
            <p className="text-xs w-72 mb-5 text-center lg:text-xl lg:w-full">
              Our teachers at Tabella are highly experienced and their passion
              to mentor and inculcate their padawans with their maximal
              knowledge is mind blowing.
            </p>
          </div>

          <GurusImg posts={posts} />
        </div>
      </div>
    </div>
  );
};

export default Gurus;
