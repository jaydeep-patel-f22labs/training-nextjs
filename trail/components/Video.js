import React, { useState } from "react";
import Image from "next/image";
import { useVideo } from "react-use";

function VideoImg(props) {
  const content = props.posts.map((post) => (
    <div key={post.index} className="h-9 w-14 mr-3.5 lg:mr-5 lg:w-28 lg:h-20">
      <Image
        src={post.src}
        width={post.width}
        height={post.height}
        alt={post.alt}
        layout="responsive"
      />
    </div>
  ));
  return (
    <div className="mt-3 pb-8 flex justify-center lg:mt-14 lg:pb-20">
      {content}
    </div>
  );
}

const posts = [
  { index: 1, src: "/img/1.jpg", alt: "video img1", width: 57, height: 36 },
  { index: 2, src: "/img/s2.jpg", alt: "video img2", width: 57, height: 36 },
  { index: 3, src: "/img/3.jpg", alt: "video img3", width: 57, height: 36 },
];

const Video = ({ videoSrc, videoPlayImg }) => {
  const [showSidebar, setSidebar] = useState(false);

  const [video, state, controls, ref] = useVideo(
    <video
      className="block lg:hidden lg:w-2/3"
      src="https://cdn.shopify.com/s/files/1/0042/2405/7462/files/Parker_Flipped.mov?1222"
    />
  );

  videoSrc = {
    src:
      "https://cdn.shopify.com/s/files/1/0042/2405/7462/files/Parker_Flipped.mov?1222",
  };

  videoPlayImg = {
    src: "/img/triangle.png",
    alt: "video play img",
    width: 22,
    height: 24,
  };

  return (
    <div className="bg-theme-grey mt-5">
      <div className="mx-4 md:mx-20">
        <div className="flex justify-center items-center flex-col">
          <p className="text-center font-bold text-xs pt-6 mb-5 lg:text-4xl lg:mb-7 lg:pt-20">
            Take a look at what we have to offer
          </p>
          <p className="text-xl text-center mb-14 hidden lg:block lg:w-11/12">
            Tabella follows a comprehensive method of learn, practice and test
            for deep immersive learning. It uses various tools like animations,
            videos and virtual labs to make learning easy and engaging. Our
            courses are designed to be entertaining, challenging, and
            educational. They are for ambitious students, professionals, and
            lifelong learners.
          </p>
        </div>

        <div className="relative lg:flex lg:justify-center">
          <video controls className="hidden lg:block lg:w-2/3">
            <source src={videoSrc.src}></source>
          </video>
          {/* <video className="block lg:hidden lg:w-2/3">
            <source src={videoSrc.src}></source>
          </video> */}

          {/* video mobile */}

          <div
            onClick={() => {
              setSidebar(!showSidebar);
            }}
          >
            <div onClick={showSidebar ? controls.pause : controls.play}>
              {video}
              <div
                className={`absolute top-1/2 right-1/2 -mt-4 transform transition-all duration-300 rotate-90 lg:hidden ${
                  showSidebar ? "opacity-0" : "opacity-100"
                }`}
              >
                <div className="w-auto h-auto">
                  <Image
                    src={videoPlayImg.src}
                    width={videoPlayImg.width}
                    height={videoPlayImg.height}
                    alt={videoPlayImg.alt}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <VideoImg posts={posts} />
      </div>
    </div>
  );
};

export default Video;
