import React, { useState } from "react";
import Image from "next/image";

const Header = ({ header }) => {
  const [showSidebar, setSidebar] = useState(false);

  header = {
    width: 91,
    height: 32,
    src: "/img/headerImg.svg",
    alt: "header img",
  };

  return (
    <div className="relative mx-4 mt-2 md:mx-20 lg:mt-0">
      <div className="absolute left-0 top-0 lg:hidden">
        <div
          onClick={() => {
            if (showSidebar == false) {
              setSidebar(!showSidebar);
            }
          }}
          className="focus:outline-none"
        >
          <div className="mt-2">
            <div className="w-4 h-0.5 bg-black mb-1"></div>
            <div className="w-4 h-0.5 bg-black mb-1"></div>
            <div className="w-4 h-0.5 bg-black mb-1"></div>
          </div>
        </div>

        {/* mobile */}
        <div
          className={`w-64 -ml-10 h-screen z-10 transform duration-300 ${
            showSidebar ? "translate-x-0 shadow-2xl" : "-translate-x-full"
          }  absolute left-0 top-0 bottom-0  transition-all bg-white lg:hidden md:-ml-20 `}
        >
          <div
            onClick={() => {
              setSidebar(!showSidebar);
            }}
            className="float-right text-sm uppercase focus:outline-none"
          >
            <div className="mt-2 mr-5">
              <div className="w-4 h-0.5 bg-black mb-1"></div>
              <div className="w-4 h-0.5 bg-black mb-1"></div>
              <div className="w-4 h-0.5 bg-black mb-1"></div>
            </div>
          </div>
          <div className="w-full ml-10 flex flex-col flex-1">
            
            <a href='#' className='pt-2.5 pb-4 text-base'>Login</a>
            <a href='#' className='pt-2.5 pb-4 text-base'>Signup</a>
            <a href='#' className='pt-2.5 pb-4 text-base'>Pricing</a>
            <a href='#' className='pt-2.5 pb-4 text-base'>Try Tabella</a>
            
          </div>
        </div>
      </div>
      <div className="flex justify-center items-center">
        {" "}
        <div className="absolute top-0 lg:left-0 lg:ml-0 lg:mt-7">
          <div className="h-8 w-24 lg:w-28">
            {" "}
            <Image
              src={header.src}
              width={header.width}
              height={header.height}
              alt={header.alt}
              layout="responsive"
            />
          </div>
        </div>
        <div className='hidden absolute right-0 top-0 lg:flex justify-center lg:mt-7'>
            <div className='font-semibold text-xl mx-10 py-1'>
              Log in
            </div>
            <div className='font-semibold text-xl px-6 py-1 rounded-lg border-2 border-black'>
              Try Tabella
            </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
