function PriceFile(props) {
  const content = props.posts.map((post) => (
    <div
      key={post.index}
      className={`relative w-32 h-40 mx-3.5 rounded bg-gradient-to-b ${post.background} lg:mx-10 lg:w-48 lg:h-60`}
    >
      <div className="ml-2.5 text-white">
        <p className="text-4xl font-black lg:text-7xl">{post.number}</p>
        <p className="uppercase text-lg font-semibold lg:text-2xl">
          {post.seats}
        </p>
        <div className="absolute bottom-0">
          <p className="text-lg font-semibold lg:hidden">
            $490.<span className="text-xs font-semibold">00 MXN</span>
          </p>
          <p className="text-xs font-semibold mb-2.5 lg:hidden">
            Billed Monthly
          </p>
          <p className="hidden text-sm font-light lg:block">Mex$</p>
          <p className="hidden text-4xl font-medium lg:block">{post.price}</p>
          <p className="hidden text-sm font-light mb-2.5 lg:block">
            Billed Monthly
          </p>
        </div>
      </div>
    </div>
  ));
  return (
    <div className="flex justify-center items-center lg:mt-16">{content}</div>
  );
}

const posts = [
  {
    index: 1,
    number: 5,
    seats: "seats",
    price: "120.88",
    background: "from-blue-light to-blue-dark",
  },
  {
    index: 2,
    number: 1,
    seats: "seat",
    price: "25.90",
    background: "from-red-light to-red-dark",
  },
];

const Prices = () => {
  return (
    <div className="p-4 lg:px-32 lg:mb-20 lg:mt-10">
      <div className="flex justify-center items-center flex-col">
        <p className="mt-7 mb-3 font-bold text-xs lg:text-4xl lg:mb-5">
          Honest prices, no hidden fees.
        </p>
        <p className="text-xs w-72 mb-5 text-center lg:text-xl lg:w-full">
          We provide you with endless supply of knowledge with complete
          financial transparency. <br />Less money, more videos
        </p>
      </div>
    

      <PriceFile posts={posts} />
      <div className="hidden justify-center items-center lg:flex">
        <div className="flex justify-center items-center rounded bg-black h-12 w-64 mt-14">
          <div className="text-white capitalize font-semibold text-xl">
            Start for free
          </div>
        </div>
      </div>
    </div>
  );
};

export default Prices;
