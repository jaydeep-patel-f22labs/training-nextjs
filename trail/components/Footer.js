const Footer = () => {
    return (
      <div className="bg-black">
        {/* mobile */}
        <div className="p-4 lg:hidden">
          <div className="text-white font-semibold text-xs leading-7 h-48">
            <p>Contact Us</p>
            <p>Blog</p>
            <p>Jobs</p>
            <p>Privacy Policy</p>
            <p>Cookies</p>
            <p>Terms of Service</p>
          </div>
          <div className="text-white font-semibold leading-7 opacity-60 text-center">
            <p style={{ fontSize: "10px" }}>
              Help & Support Instagram Facebook Twitter Pinterest
            </p>
          </div>
          <div className="text-white text-center font-semibold">
            <p style={{ fontSize: "10px" }}>© 2018 Tabella Limited</p>
          </div>
        </div>
  
        {/* laptop */}
        <div className="hidden px-32 pt-14 pb-4 lg:block">
          <div className="flex justify-start text-white font-semibold text-base leading-7 ">
            <div className="h-40">
              <p>Contact Us</p>
              <p>About Us</p>
              <p>Team</p>
              <p>Privacy Policy</p>
              <p>Cookies</p>
            </div>
            <div className="ml-16">
              <p>Terms of Service</p>
              <p>Learning & Methodology</p>
              <p>Pricing</p>
            </div>
          </div>
          <div className="flex">
            <div className="flex-1 mt-14 text-white font-semibold leading-7 opacity-60 text-sm">
              <p>Help & Support Instagram Facebook Twitter Pinterest</p>
            </div>
            <div className="flex-1 text-white mt-14 font-semibold text-sm">
              <p>© 2018 Tabella Limited</p>
            </div>
          </div>
        </div>
      </div>
    );
  };
  
  export default Footer;
  