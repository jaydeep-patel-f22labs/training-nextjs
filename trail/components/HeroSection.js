import Image from "next/image";

const HeroSection = ({ img }) => {
  img = {
    width: 166,
    height: 125,
    src: "/img/34767.svg",
    alt: "herosection img",
  };

  return (
    <div className="mx-4 md:mx-20 lg:mx-0">
      {/* mobile */}
      <div className="flex items-center justify-between flex-col lg:hidden">
        <div className="mt-20 h-auto w-40">
          <Image
            src={img.src}
            width={img.width}
            height={img.height}
            alt={img.alt}
            layout="responsive"
          />
        </div>
        <div className="w-60 font-bold mt-9">
          <p className="text-sm text-center font-sans1">
            LEARNING MADE EASY & ENGAGING
          </p>
        </div>
        <div>
          <p className="text-xs w-72 mb-5 text-center mt-3.5">
            Tabella helps you learn more effectively than lectures through
            interactive problem solving courses. Our courses are designed to be
            entertaining, challenging, and educational. They are for ambitious
            students, professionals, and lifelong learners.
          </p>
        </div>
        <div className="flex justify-center items-center rounded bg-black h-9 w-36">
          <div className="text-white capitalize font-semibold text-base">
            try tabbella
          </div>
        </div>
      </div>

      {/* laptop */}
      <div className="hidden lg:flex">
        <div className="flex-1 flex justify-center items-center w-1/2">
          <div className="mx-20 text-left">
            <p className="tracking-wide mt-36 uppercase text-4xl font-Poppins">
              LEARNING MADE EASY & ENGAGING
            </p>
            <p className="text-xl font-medium mt-5">
              Tabella helps you learn more effectively than lectures through
              interactive problem solving courses. Our courses are designed to
              be entertaining, challenging, and educational. They are for
              ambitious students, professionals, and lifelong learners.
            </p>
            <div className="flex justify-center items-center rounded bg-black h-12 w-64 mt-8">
              <div className="text-white capitalize font-semibold text-lg">
                try tabbella for free
              </div>
            </div>
          </div>
        </div>
        <div className="flex-1 overflow-x-hidden bg-hero-pattern w-1/2">
          <div className="mr-20 mt-36">
           
            <Image
              src={img.src}
              width={img.width}
              height={img.height}
              alt={img.alt}
              layout="responsive"
              
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeroSection;
