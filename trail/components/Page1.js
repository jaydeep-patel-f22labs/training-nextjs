import Header from "./Header";
import HeroSection from "./HeroSection";
import Video from "./Video";
import Mobile from "./Mobile";
import Gurus from "./Gurus";
import Prices from "./Prices";
import Learn from "./Learn";
import Footer from "./Footer";
import VideoT from "./Test"

const Page1 = () => {
  return (
    <>
   
      <Header />
      <HeroSection />
      <Video />
      <Mobile />
      <Gurus />
      <Prices />
      <Learn />
      <Footer />
      
      
    </>
  );
};

export default Page1;
