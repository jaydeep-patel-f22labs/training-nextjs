module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "theme-blue": "#1c1794",
        "theme-grey": "#fafafa",
        "blue-dark": "#77b6ff",
        "blue-light": "#81c6fc",
        "red-light": "#f57c8a",
        "red-dark": "#ef2353",
      },
      backgroundImage: (theme) => ({
        "hero-pattern": "url('/img/group-3.svg')",
        
      }),
      fontSize: {
        '1/2': '0.5rem',
       }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  fontFamily: {
    ProximaNova: ["ProximaNova"],
    Poppins: ["Poppins"],
  },
}
