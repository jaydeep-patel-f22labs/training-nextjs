import Head from 'next/head'
import Page1 from '../components/Page1'

export default function Home() {
  return (
    <>
    <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
        <meta charSet="UTF-8" />
        <meta name="description" content="Free Web tutorials" />
        <meta name="keywords" content="HTML,CSS,XML,JavaScript" />
        <meta name="author" content="John Doe" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
    <div className='font-ProximaNova'>
    <Page1 />
    </div>
    </>
  )
}
